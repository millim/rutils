require 'optparse'
require_relative "repository"
options = {
  dir: '.'
}

OptionParser.new do |opts|
  opts.banner = "Usage: main.rb [options]"

  opts.on("-m", "--multiple", TrueClass, "is many dir") do |v|
    options[:multiple] = v
  end

  opts.on("-d DIR", "--dir=DIR", String, "Select Dir") do |v|
    options[:dir] = v
  end

  opts.on("-t TOKEN", "--token=TOKEN", String, "gitee.com all permissions token") do |v|
    options[:token] = v
  end

  opts.on("-n NAME", "--name=NAME", String, "gitee.com repository user name") do |v|
    options[:name] = v
  end

end.parse!

at_exit do
  puts "run end"
end

if options[:token].nil? || options[:token].empty?
  puts "You need input gitee.com token"
  exit
end

if options[:name].nil? || options[:name].empty?
  puts "You need input gitee.com repository user name"
  exit
end

unless Dir.exists? options[:dir]
  puts "You need select dir"
  exit
end

if options[:multiple]
  dirs = Dir.foreach(options[:dir]).select{|f| f != '.' && f != '..'}.select{|f| Dir.exists?("#{options[:dir]}/#{f}")}.map{|f| "#{options[:dir]}/#{f}"}
else
  dirs = [options[:dir]]
end
rep = Repository.new dirs, options[:name], options[:token]
rep.auto_run

